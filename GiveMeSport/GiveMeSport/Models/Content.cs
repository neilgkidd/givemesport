﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GiveMeSport.Models
{
    public class Content
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Link { get; set; }

        public DateTime PublishedDate { get; set; }

        public string Comments { get; set; }
    }
}