﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GiveMeSport.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace GiveMeSport.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            string apiURI = HttpContext.Request.Url + "api/content/latest";            
            
            return View("index",
                await GetArticlesAsync(apiURI)
            );
        }

        public async Task<ICollection<Content>> GetArticlesAsync(string uri)
        {            
            using (HttpClient httpClient = new HttpClient())
            {
                return JsonConvert.DeserializeObject<ICollection<Content>>(
                    await httpClient.GetStringAsync(uri)
                );
            }
        }

        public ActionResult BreakingNews()
        {
            DataController data = new DataController();
            var articles = data.GetBreakingNews(5);

            return PartialView("BreakingNews", articles);
        }
    }
}