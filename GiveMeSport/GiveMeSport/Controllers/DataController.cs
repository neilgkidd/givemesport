﻿using GiveMeSport.DAL;
using GiveMeSport.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GiveMeSport.Controllers
{
    public class DataController : ApiController
    {
        IRSSContext context;
        IContentRepository repository;

        public DataController()
        {
            context = new RSSContext("http://www.givemesport.com/rss.ashx");
            repository = new ContentRepository(context);
        }
       
        [HttpGet]
        [Route("~/api/content")]
        public IEnumerable<Content> GetAllContent()
        {
            return repository.GetAllContent();
        }

        [HttpGet]
        [Route("~/api/content/latest")]
        public ICollection<Content> GetLatestArticles()
        {
            //default number - should be moved in configuration or database
            int numberOfLatestArticles = 10;
            
            return repository.GetLatestArticles(numberOfLatestArticles);
        }

        [HttpGet]
        [Route("~/api/content/breaking/{numberOfMinutes}")]
        public ICollection<Content> GetBreakingNews(int numberOfMinutes)
        {
            return repository.GetBreakingNews(numberOfMinutes);
        }
    }
}
