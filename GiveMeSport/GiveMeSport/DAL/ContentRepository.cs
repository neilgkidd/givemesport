﻿using GiveMeSport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GiveMeSport.DAL
{
    public class ContentRepository : IContentRepository
    {
        private IRSSContext context;

        public ContentRepository(IRSSContext rssContext)
        {
            this.context = rssContext;
        }

        public IEnumerable<Content> GetAllContent()
        {
            return context.RSSFeed.Descendants("item").Select(c => new Content {    Title = c.Element("title").Value,
                                                                                    Description = c.Element("description").Value,
                                                                                    Link = c.Element("link").Value,
                                                                                    PublishedDate = Convert.ToDateTime(c.Element("pubDate").Value),
                                                                                    Comments = c.Element("comments").Value,
                                                                                });
        }

        public ICollection<Content> GetLatestArticles(int numArticles)
        { 
            return GetAllContent().OrderByDescending(i => i.PublishedDate).Take(numArticles).ToList();
        }

        public ICollection<Content> GetBreakingNews(int numMinutes)
        {
            return GetAllContent().Where(i => i.PublishedDate > DateTime.Now.AddMinutes(-numMinutes)).ToList();
        }
    }
}