﻿using GiveMeSport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GiveMeSport.DAL
{
    public interface IContentRepository
    {
        IEnumerable<Content> GetAllContent();

        ICollection<Content> GetLatestArticles(int numArticles);

        ICollection<Content> GetBreakingNews(int numMinutes);
    }
}