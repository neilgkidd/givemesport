﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace GiveMeSport.DAL
{
    public class RSSContext : IRSSContext
    {
        public RSSContext(string rssPath)
        {
            this.RSSFeed = XDocument.Load(rssPath);
        }

        public XDocument RSSFeed { get; set; }
    }
}