﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace GiveMeSport.DAL
{
    public interface IRSSContext
    {
        XDocument RSSFeed { get; set; }
    }
}