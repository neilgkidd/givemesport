﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GiveMeSport.DAL;
using GiveMeSport.Models;
using System.Collections.Generic;

namespace GiveMeSport.Tests.DAL
{
    [TestClass]
    public class RSSContextTest
    {
        IRSSContext context;
        IContentRepository repository;

        public RSSContextTest()
        {
            context = new RSSContext("http://www.givemesport.com/rss.ashx");
            repository = new ContentRepository(context);
        }

        [TestMethod]
        public void GetAllContent()
        {
            var testCollection = repository.GetAllContent();  
            
            Assert.IsNotNull(testCollection);
        }

        [TestMethod]
        public void GetLatestArticles()
        {
            int numberOfArticles = 10;
            var testCollection = repository.GetLatestArticles(numberOfArticles);            

            Assert.IsTrue(testCollection.Count == numberOfArticles);
        }

        [TestMethod]
        public void GetBreakingNews()
        {
            int numMinutes = 5;
            var testCollection = repository.GetBreakingNews(numMinutes);

            Assert.IsNotNull(testCollection);
        }
    }
}
