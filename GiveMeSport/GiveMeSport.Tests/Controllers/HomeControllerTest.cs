﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GiveMeSport;
using GiveMeSport.Controllers;

namespace GiveMeSport.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            var result = controller.Index();

            // Assert - Expect to receive fault as API isn't running
            Assert.IsTrue(result.Status == System.Threading.Tasks.TaskStatus.Faulted);
        }
    }
}
