﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GiveMeSport.DAL;
using GiveMeSport.Controllers;
using System.Collections.Generic;
using GiveMeSport.Models;

namespace GiveMeSport.Tests.Controllers
{
    [TestClass]
    public class DataControllerTest
    {
        [TestMethod]
        public void APIGetAllContent()
        {
            IRSSContext context = new RSSContext("http://www.givemesport.com/rss.ashx");
            IContentRepository repository = new ContentRepository(context);

            DataController controller = new DataController();

            IEnumerable<Content> result = controller.GetAllContent();            

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void APIGetLatestArticles()            
        {
            IRSSContext context = new RSSContext("http://www.givemesport.com/rss.ashx");
            IContentRepository repository = new ContentRepository(context);

            DataController controller = new DataController();

            ICollection<Content> result = controller.GetLatestArticles();

            Assert.IsNotNull(result.Count);
        }

        [TestMethod]
        public void APIGetBreakingNews()
        {
            IRSSContext context = new RSSContext("http://www.givemesport.com/rss.ashx");
            IContentRepository repository = new ContentRepository(context);

            DataController controller = new DataController();
            int numberOfMinutes = 60; //use larger number for testing

            ICollection<Content> result = controller.GetBreakingNews(numberOfMinutes);

            Assert.IsNotNull(result.Count);
        }
    }
}
